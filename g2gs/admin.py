from django.contrib import admin
from g2gs.models import Venue, Event
from django.forms import ModelForm
from floppyforms.gis import PointWidget, BaseGMapWidget

class CustomPointWidget(PointWidget, BaseGMapWidget):
  google_maps_api_key = 'AIzaSyCoq4yITgSP4hvDrWEN_E8XFMRRi4SMPNM'
  #google_maps_api_key = settings.GOOGLE_MAPS_API_KEY
#class CustomPointWidget(PointWidget, BaseOsmWidget):
  class Media:
    js = ('/static/floppyforms/js/MapWidget.js',)

class VenueAdminForm(ModelForm):
  class Meta:
    model = Venue
    fields = ['name', 'location']
    widgets = {
      'location': CustomPointWidget()
    }
        
class VenueAdmin(admin.ModelAdmin):
  form = VenueAdminForm
    
admin.site.register(Venue, VenueAdmin)
admin.site.register(Event)