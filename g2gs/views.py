from django.shortcuts import render, render_to_response
from django.views.generic.edit import FormView
from g2gs.forms import LookupForm
from g2gs.models import Event
from django.utils import timezone
from django.contrib.gis.geos import Point
from django.contrib.gis.db.models.functions import Distance


class LookupView(FormView):
  form_class = LookupForm # wieso ohne (request.POST)??
  
  def get(self, request):
    #return render(request, '/home/redmine/WebDev/G2/templates/lookup.html')
    return render(request, 'lookup.html')

  def form_valid(self,form):
    #get data
    latitude = form.cleaned_data['latitude']
    longitude = form.cleaned_data['longitude']
    #get todays date
    now = timezone.now()
    #get next weeks date
    next_week = now + timezone.timedelta(weeks=1)
    #get point
    location = Point(longitude,latitude,srid=4326)
    #look up events
    events = Event.objects.filter(datetime__gte=now).filter(datetime__lte=next_week).annotate(distance=Distance('venue__location', location)).order_by('distance')[0:5]
    # Render the template
#    return render('/home/redmine/WebDev/G2/templates/lookupresults.html', {
 #     'events': events
  #  })
    return render_to_response('lookupresults.html', {'events': events})
