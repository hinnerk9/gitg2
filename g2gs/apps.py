from django.apps import AppConfig


class G2GsConfig(AppConfig):
    name = 'g2gs'
