from django.conf.urls import url
from django.contrib import admin
from g2gs.views import LookupView

urlpatterns = [
    url(r'^$', LookupView.as_view(), name='lookup'),
    #url(r'^lookupresults/', view.resView(), name='resView'),
    url(r'^admin/', admin.site.urls),
]
